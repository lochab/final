import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import tests.Cart;
import tests.Product;

class CartTest {
	private static int score = 0;

	@Test
	void testGetCart() {
		Cart c=new Cart();
		Product p=new Product();
		c.addProduct(p);
		c.getCartSize();
		assertEquals(1, c.getCartSize());

		p=new Product();
		c.addProduct(p);
		c.getCartSize();
		assertEquals(2, c.getCartSize());
	}