
public abstract class Discount {
	double discount;
	abstract double calculateDiscount(double amount);  
}
